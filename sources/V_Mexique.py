# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:28:51 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_radio_V_Mexique.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/Lambiaze.webp" alt="Lanbiaze"> 
        </div> <div class="details"> 
        <h3>JP</h3> <h4>Mexique GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>Nous sommes au départ tu est deuxième veux-tu doubler Hamilton au premier virage ou attendre plus tard ?</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="V_Mexique_P1.py" class="btn mt-4" >Tout de suite</a></li> <li><a href="V_Mexique_P2.py" class="btn mt-4" >Attendre</a></li>  </ul> </div> </div> 
</body>
"""

print(html)
#ressources utiles "front.codes"
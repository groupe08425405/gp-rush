# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:28:51 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_radio_V_Abu_Dhabi.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/Lambiaze.webp" alt="Lanbiaze"> 
        </div> <div class="details"> 
        <h3>JP</h3> <h4>Abu Dhabi GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>Une voiture a eu un accident et le drapeau rouge est sortis il restent trois tour, veux-tu tout tenter au restart ou attendre une erreur d'Hamilton ?</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="V_Abu_Dhabi_P1.py" class="btn mt-4" ></a></li> Tout donner<li><a href="V_Abu_Dhabi_P2.py" class="btn mt-4" >Attendre une erreur</a><li>Garder les pneus</li>  </ul> </div> </div> 
</body>
"""

print(html)
#ressources utiles "front.codes"
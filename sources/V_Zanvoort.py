# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:28:51 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_radio_V_Zanvoort.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/Lambiaze.webp" alt="Lanbiaze"> 
        </div> <div class="details"> 
        <h3>JP</h3> <h4>Zanvoort GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>Avant la course nous avons changer des pieces sur la voiture et nous a valu une pénalité du part au fond de la grille, veux-tu tenter les dépassemnets ou assurer de finir la course ? </p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="V_Zanvoort_P2.py" class="btn mt-4" >Tenter les dépassemnets</a></li> <li><a href="V_Zanvoort_P5.py" class="btn mt-4" >Assurer la course</a></li>  </ul> </div> </div> 
</body>
"""

print(html)
#ressources utiles "front.codes"
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 08:10:29 2024

@author: hpiron
"""

import random
import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()

print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_H_Resume.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/H_Victoire.webp" alt="victoire"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Mexique GP</h4> <h4> Résumé</h4> 
    <p>Tu as réussi à bloquer Verstappen derière toi toute la course tu termine donc premier on se donne rendez-vous pour la dernière course à Abu Dhabi</p> <h4>Prochain Grand Prix</h4> <ul class="size"> 
    <li><a href="H_Abu_Dhabi.py" class="btn mt-4" > Aller à Abu Dhabi</a></li>  </ul> </div> </div> 
</body>
</html>
"""
print(html)
#ressources utiles "front.codes"
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 08:10:29 2024

@author: hpiron
"""

import random
import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()

print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_radio_H_Bahrein.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="../Images/Bono.jpg" alt="Bono"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Bahrain GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>On est dans le dernier virage du dernier tour veut tu rester derrière Verstappen et attendre une erreur de sa part ou tenter de le dépassé à l'intérieur</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="H_Bahrein_P1.py" class="btn mt-4" >Intérieur</a></li> <li><a href="H_Bahrein_P2.py" class="btn mt-4" >Rester sur la trajectoire</a></li>  </ul> </div> </div> 
</body>
</html>
"""
print(html)
#ressources utiles "front.codes"

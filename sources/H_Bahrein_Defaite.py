# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 08:10:29 2024

@author: hpiron
"""

import random
import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()

print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_H_Resume.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/H_Défaite.jpg" alt="Défaite"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Bahrain GP</h4> <h4> Résumé</h4> 
    <p>Tu as deuxième derrière Verstappen je suis sur que la prochaine fois tu arrivera devant lui.</p> <h4>Prochain Grand Prix</h4> <ul class="size"> 
    <li><a href="H_Baku.py" class="btn mt-4" > Aller à Baku</a></li>  </ul> </div> </div> 
</body>
</html>
"""
print(html)
#ressources utiles "front.codes"
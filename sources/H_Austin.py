# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:55:50 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
  <link rel="stylesheet" type"text/css" href="Styles/style_radio_H_Austin.css"> 
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="../Images/Bono.jpg" alt="Bono"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Austin GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>On vient de faire un arrêt au stand tu sors derrière Verstappen veux-tu pousser pour le dépasser ou assurer l'usure des pneus</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="H_Austin_P3.py" class="btn mt-4" >Pousser</a></li> <li><a href="H_Austin_P2.py" class="btn mt-4" >Assurer</a></a></li>  </ul> </div> </div> 
    </body> </html>
"""
print(html)
#ressources utiles "front.codes"
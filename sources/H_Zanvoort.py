# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:12:34 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
  <link rel="stylesheet" type"text/css" href="Styles/style_radio_H_Zanvoort.css"> 
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="../Images/Bono.jpg" alt="Bono"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Zanvoort GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>On pense que la pluie arrivera trés fortement au prochain tour veux-tu changer les pneus maintenant ou attendre le prochain tour</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="H_Zanvoort_P1.py" class="btn mt-4" >Tout de suite</a></li> <li><a href="H_Zanvoort_Crash.py" class="btn mt-4" >Attendre le prochain tour </a></a></li>  </ul> </div> </div> 
    </body> </html>
"""
print(html)
#ressources utiles "front.codes"
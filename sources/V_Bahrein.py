# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:28:51 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_radio_V_Bahrein.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/Lambiaze.webp" alt="Lanbiaze"> 
        </div> <div class="details"> 
        <h3>JP</h3> <h4>Bahrain GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>On est dans le dernier virage du dernier tour veut tu bloquer Hamilton ou rester sur ta trajectoire</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="V_Bahrein_P1.py" class="btn mt-4" >Bloquer</a></li> <li><a href="V_Bahrein_P2.py" class="btn mt-4" >Rester sur la trajectoire</a></li>  </ul> </div> </div> 
</body>
"""

print(html)
#ressources utiles "front.codes"
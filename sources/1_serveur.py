# -*- coding: utf-8 -*-
"""
Created on Fri Jan 12 09:45:23 2024

@author: ppideill
"""

import http.server
port = 80
address=("", port)

server = http.server.HTTPServer
handler = http.server.CGIHTTPRequestHandler
handler.cgi_directories=["/"]

httpd = server(address, handler)
print(f"Serveur démarré sur le PORT{port}")
httpd.serve_forever()
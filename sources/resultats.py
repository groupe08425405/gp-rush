# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:57:18 2024

@author: ppideill
"""

import csv
import cgi
# Le module qui permet de traiter les données de formulaires
import cgitb
import csv

# on active le mode débogage
cgitb.enable()
# Avec le module cgi, on récupère les données du formulaire
form = cgi.FieldStorage()

html = """<!DOCTYPE html>
<html lang="fr">
"""

print(html)

print("""<head>
          <meta charset="UTF-8">
          <title> Ma page Web </title>
 </head>
 <body>
 """)
 
# On ne fait pas confiance à l'utilisateur
try : # on essaie : si ça marche, RAS
 if form.getvalue("username"): #si l'utilisateur a bien rempli le champ username
     username = form.getvalue("username") # on enregistre la valeur transmise
     print(f"""<h1> Page de resultats </h1><p>Bonjour {username}!</p>""") # on affiche son nom de façon conviviale

 else :
     raise Exception #sinon, on lève une exception
except: # Si une exception a été levée
 print("<p>Pseudo non transmis</p>") # On informe l'utilisateur
 
 # On ne fait pas confiance à l'utilisateur
try : # on essaie : si ça marche, RAS
 if form.getvalue("age"): #si l'utilisateur a bien rempli le champ age
     age = form.getvalue("age") # on enregistre la valeur transmise
     print(f"<p>Tu as {age} ans!</p>") # on affiche son age de façon conviviale
 else :
     raise Exception #sinon, on lève une exception
except: # Si une exception a été levée
 print("<p>age non transmis</p>") # On informe l'utilisateur

 
#on oublie pas de terminer le code de sa page HTML
html="""
</body>
</html>"""
print(html)
# Les données que nous allons écrire
data = (username,age)
# Ouvrir le fichier en mode ajout (et crée le fichier s'il n'existe pas !)
fichier = open('resultats.csv','a')
# Créer l'objet fichier
obj = csv.writer(fichier)
# Chaque valeur de data correspond à une nouvelle ligne
obj.writerow(data)
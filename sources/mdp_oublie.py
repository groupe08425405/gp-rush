# -*- coding: utf-8 -*-

"""
Created on Fri Jan 19 18:23:10 2024

@author: ppideill
"""

import random
import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()
    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
"""
print(html)

print("""<head>
<title> GP Rush ! </title>
    <link rel="stylesheet" href="Styles/style_mdp.css">
</head>
<body style="background-color:#666E76;">
<center>
<br><br>
<h1> Vous avez oublié votre mot de passe ? Aucun problème !</h1>

<p> Nous venons de générer un nouveau mot de passe spécialement conçu pour vous. </p>
<p> Effectivement, nous avons utilisé un système qui permet de créer de nouveaux mots de passe aléatoirement composés de 4 chiffres. </p>
<p> Ainsi, votre nouveau mot de passe est unique et plus sur ! </p> 
</center>
""")

a=random.randint(1,9)
b=random.randint(1,9)
c=random.randint(1,9)
d=random.randint(1,9)

print(f'<p><center> Votre nouveau mot de passe est : {a}{b}{c}{d}</center></p>')

html="""
</br>
<center>
<form method="post" action="index.py">
<input type="submit" value="Se reconnecter" class="button-mdp">
</form>
</br></br>
</body>
<hr></hr>
<footer>
<br><br>
<p> Directeur de publication : GP Rush ! </p>
<p> Webmestre : L'équipe Paul Hugo et Ilyan</p>
<br><br>
<img src="Images/logo.PNG" alt="logo GP Rush !" title="logo" class="logo" />
</center>
</footer>
</html>
 """
print(html)
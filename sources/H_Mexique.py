# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:55:50 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
  <link rel="stylesheet" type"text/css" href="Styles/style_radio_H_Mexique.css"> 
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="../Images/Bono.jpg" alt="Bono"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Mexique GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>On est départ de la course veux-tu couvrir l'extérieur du premier virage ou la trajectoire optimal ?</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="H_Mexique_P2.py" class="btn mt-4" >Protéger la trajectoire</a></li> <li><a href="H_Mexique_P1.py" class="btn mt-4" >Protéger l'extérieur</a></a></li>  </ul> </div> </div> 
    </body> </html>
"""
print(html)
#ressources utiles "front.codes"
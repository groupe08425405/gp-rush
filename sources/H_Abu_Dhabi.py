# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:55:50 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
  <link rel="stylesheet" type"text/css" href="Styles/style_radio_H_Abu_Dhabi.css"> 
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="../Images/Bono.jpg" alt="Bono"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Abu Dhabi GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>Un pilote a eu un accident le drapeau rouge est sorti, veux-tu tout donner ou assurer la trajectoire </p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="H_Abu_Dhabi_P1.py" class="btn mt-4" >Tout donner</a></li> <li><a href="H_Abu_Dhabi_P2.py" class="btn mt-4" >Assurer la trajectoire</a></a></li>  </ul> </div> </div> 
    </body> </html>
"""
print(html)
#ressources utiles "front.codes"
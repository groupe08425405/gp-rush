# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 08:10:29 2024

@author: hpiron
"""

import random
import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()

print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_H_Resume.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/H_Victoire.webp" alt="victoire"> 
        </div> <div class="details"> 
        <h3>Bono</h3> <h4>Zanvoort GP</h4> <h4> Résumé</h4> 
    <p>Grace à notre stratégie nous avons rattraper tout le monde en difficulté sous la pluie merci de nous avoir fait confiance on se retrouve à Austin</p> <h4>Prochain Grand Prix</h4> <ul class="size"> 
    <li><a href="H_Austin.py" class="btn mt-4" > Aller à Austin</a></li>  </ul> </div> </div> 
</body>
</html>
"""
print(html)
#ressources utiles "front.codes"
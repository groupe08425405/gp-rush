# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:28:51 2024

@author: hpiron
"""

import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html> 
<html lang="fr"> 
<head> <title> GP Rush ! </title> 
    <link rel="stylesheet" type"text/css" href="Styles/style_radio_V_Baku.css">
</head>
<body> 
    <div class="card"> 
        <div class="imgBx"> 
            <img src="Images/Lambiaze.webp" alt="Lanbiaze"> 
        </div> <div class="details"> 
        <h3>JP</h3> <h4>Baku GP</h4> <h4>Que veux tu faires ?</h4> 
    <p>Tu es premier avec beaucoup d'avance veut inscrire ton nom dans l'histoire en tentant des records ou rester tranquille</p> <h4>Ton choix</h4> <ul class="size"> 
    <li><a href="V_Baku_P1.py" class="btn mt-4" >Rester calme</a></li> <li><a href="V_Baku_Crash.py" class="btn mt-4" >Tenter les records</a></li>  </ul> </div> </div> 
</body>
"""

print(html)
#ressources utiles "front.codes"
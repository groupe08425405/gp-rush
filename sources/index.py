# -*- coding: utf-8 -*-
"""
Created on Fri Jan 19 08:55:50 2024

@author: ppideill
"""
import pandas

print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
<title> GP Rush ! </title>
    <link rel="stylesheet" type"text/css" href="Styles/index.css">
    <link rel="icon" href="Images/logo.PNG">
    <style type="text/css">
      html, html *
      {
        position:relative;
      }
 
      body
      {
        margin: 0;
      }
 
      .img_fond
      {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
    </style>
  </head>
 
  <body>
    <div class="img_fond">
      <img class="img_fond" src="https://e0.365dm.com/21/12/1600x900/skysports-f1-abu-dhabi-finale_5607472.png?20211207160333" alt="background"/>
    </div>
<div>
<center>
<h1>Bienvenue sur GP Rush !</h1>
<br><br><br><br><br><br><br>
	<a class="logo" target="_blank"></a>
	<div class="section">
		<div class="container">
			<div class="row full-height justify-content-center">
				<div class="col-12 text-center align-self-center py-5">
					<div class="section pb-5 pt-5 pt-sm-2 text-center">
						<h6 class="mb-0 pb-3"><span>Se connecter </span><span>S'inscrire</span></h6>
			          	<input class="checkbox" type="checkbox" id="reg-log" name="reg-log"/>
			          	<label for="reg-log"></label>
						<div class="card-3d-wrap mx-auto">
							<div class="card-3d-wrapper">
								<div class="card-front">
									<div class="center-wrap">
										<div class="section text-center">							
											<h4 class="mb-4 pb-3">Se connecter</h4>
											<div class="form-group">
                                                    <form method="post" action="resultats.py">
												<input type="text" name="username" class="form-style" placeholder="Votre Pseudo" id="logemail2" autocomplete="off"required>
											</div>	
											<div class="form-group mt-2">
												<input type="password" name="logpass" class="form-style" placeholder="Votre mot de passe" id="logpass2" autocomplete="off"required>
                                            </form>
											</div>
                                            <br>
											<a href="choix_perso.py" class="btn mt-4">Envoyer</a>
                            				<p class="mb-0 mt-4 text-center"><a href="mdp_oublie.py" class="link">Mot de passe oublié ?</a></p>
				      					</div>
			      					</div>
			      				</div>
								<div class="card-back">
									<div class="center-wrap">
										<div class="section text-center">
											<h4 class="mb-4 pb-3">S'inscrire</h4>
											<div class="form-group">
                                            <form method="get" action="formulaire.php">
												<input type="text" name="logname" class="form-style" placeholder="Votre Email" id="logname" autocomplete="off">
											</div>	
											<div class="form-group mt-2">
                                                <input type="email" name="logemail" class="form-style" placeholder="Votre Pseudo" id="logemail" autocomplete="off"required>
											</div>	
											<div class="form-group mt-2">
												<input type="password" name="logpass" class="form-style" placeholder="Votre Mot de Passe" id="logpass" autocomplete="off"required>
                                            </form>
											</div>
                                            <br>
											<a href="" class="btn mt-4">Envoyer</a>
				      					</div>
			      					</div>
			      				</div>
			      			</div>
			      		</div>
			      	</div>
		      	</div>
	      	</div>
	    </div>
	</div>
</center>
</form>
</body>
</html>
"""
print(html)
#ressources utiles "front.codes"

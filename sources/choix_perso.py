# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:01:11 2024

@author: ACER
"""
  
import csv
import cgi
import cgitb
cgitb.enable()
form = cgi.FieldStorage()


    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
    <title> GP Rush ! </title>
    <link rel="icon" href="Images/logo.PNG">
    <link rel="stylesheet" type"text/css" href="Styles/style_choix_perso_base.css">
    <style type="text/css">
      html, html *
      {
        position:relative;
      }
 
      body
      {
        margin: 0;
      }
 
      .img_fond
      {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
      }
    </style>
</head>
<body>
    <div class="img_fond">
      <img class="img_fond" src="Images/fond_choix_perso.jpg" alt="background"/>
    </div>
</body>
    <center>
        <h1> Faites votre choix ! </h1>
        <h2> Incarnerez vous le célébre Max Verstappen ou l'iconique Lewis Hamilton ? </h2>
    <br><br>
<footer>
    <pre>
    <a href="V_Intro.py" class="btn" style="width: 21%">Max Verstappen</a>               <a href="H_Intro.py" class="btn" style="width: 21%">Lewis Hamilton</a>
    </pre>
    </center>
</footer>
</html>
"""
print(html)
#ressources utiles "front.codes"

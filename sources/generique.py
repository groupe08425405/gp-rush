# -*- coding: utf-8 -*-
"""
Created on Tue Mar  5 18:24:59 2024

@author: ACER
"""
    
print("Content-type: text/html; charset=UTF_8\n")

html = """<!DOCTYPE html>
<html lang="fr">
<head>
    <title> GP Rush ! </title>
    <link rel="icon" href="Images/logo.PNG">
    <link rel="stylesheet" type"text/css" href="Styles/style_generique.css">
</head>
<body>
<center>
    <div class="a-long-time-ago">
        Merci d'avoir joué à notre jeu.
    </div>

    <div class="crawl">
        <div>
            <p>
                Dans l'arène du bitume, où rugissent les moteurs,
            </p>
            <p>
                Un pilote navigue, à travers les virages en pleurs.
            </p>
            <p>
                Dans son cockpit, il entend murmurer l'Univers,
            </p>
            <p>
                Chuchotant des vérités, des mystères, des airs.
            </p>
            <br>
            <p>
                Il a atteint des sommets, il est temps que tu le regardes.
            </p>
            <p>
                Il peut lire dans nos pensées, comme des mots sur un écran,
            </p>
            <p>
                Mais pour lui, nous ne sommes que des concurents."
            </p>
            <br>
            <p>
                "Qu'a rêvé ce joueur, dans ses nuits agitées ?
            </p>
            <p>
                De mondes de vitesse, de dépassement et de fumée.
            </p>
            <p>
                Il a rêvé qu'il démarait, qu'il dérapait, qu'il crissait,
            </p>
            <p>
                Mais parfois, il erre dans des ténèbres, où l'espoir s'efface."
            </p>
            <br>
            <p>
                "Nous sommes son univers, ses rêves, ses peurs,
            </p>
            <p>
                Tissés dans le fil du temps, où se mêlent pleurs et bonheur.
            </p>
            <p>
                Il est l'amour, il est la lumière, dans ce jeu sans fin,
            </p>
            <p>
               Et à travers ses rêves, il façonne son destin."
            </p>
            <br>
            <p>
                "Réveille-toi, Charles, dans l'arène des pistes,
            </p>
            <p>
                Tu es le joueur, le maître de tes propres drifts.
            </p>
            <p>
                Prends conscience de ton pouvoir, de ton rôle essentiel,
            </p>
            <p>
                Dans ce rêve infini, où se joue l'éternel."
            </p>
            <br>
            <p>
                "Tu es le pilote Ferrari, réveille-toi, prends une bouffée d'air,
            </p>
            <p>
                Sens la piste sous tes roues, laisse-toi transporter.
            </p>
            <p>
                Tu es l'univers, tu es l'amour, dans cette symphonie,
            </p>
            <p>
                Où chaque tour de piste, te rapproche de l'infini."
            </p>
            <br>
            <p>
                "Réveille-toi ! Réveille-toi, dans ce jeu sans fin,
            </p>
            <p>
                Où se mêlent réalité et rêve, destin et destinée.
            </p>
            <p>
                Tu es le futur, réveille-toi, et rêve mieux,
            </p>
            <p>
                Dans l'arène de la vie, où se joue ton propre jeu."
            </p>
        </div>
    </div>
</center>
</body>
</html>
"""
print(html)